﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Wethr_Tlgrm_Bot
{
    class WeatherInfo
    {
        [JsonProperty(PropertyName = "Description")]
        public string Description { get; set; }
    }
}
