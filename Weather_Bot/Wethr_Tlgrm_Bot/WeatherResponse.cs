﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wethr_Tlgrm_Bot
{
    class WeatherResponse
    {
        public TemperatureInfo Main { get; set; }
        
        public List<WeatherInfo> Weather { get; set; }

        public string Name { get; set; }

        public string id { get; set; }

        
    }
}
