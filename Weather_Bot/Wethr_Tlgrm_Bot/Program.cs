﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types.ReplyMarkups;

namespace Wethr_Tlgrm_Bot
{
    class Program
    {
        private static string token { get; set; } = "2049496282:AAFkd4_4kOrBpAF4aYNiBjZHdto3loiqynQ";
        private static TelegramBotClient client;


        static void Main(string[] args)
        {
            client = new TelegramBotClient(token);
            client.StartReceiving();
            client.OnMessage += OnMessageHandler;
            Console.ReadLine();
            client.StopReceiving();
        }

        private static async void OnMessageHandler(object sender, MessageEventArgs e)
        {
            bool wea = true;
            var msg = e.Message;

            string i = msg.Text;
           
            if(msg!=null)
            {
                Console.WriteLine($"Пришло сообщение с текстом: {msg.Text}");

                    if(msg.Text == "Привет")
                {
                    wea = false;
                    var stic = await client.SendStickerAsync(
                        chatId: msg.Chat.Id,
                        sticker: "https://tlgrm.ru/_/stickers/5a7/cb3/5a7cb3d0-bca6-3459-a3f0-5745d95d54b7/1.webp",
                        replyToMessageId: msg.MessageId, replyMarkup: GetButtons());
                    wea = true;
                }
                        
                    else if(msg.Text=="Что ты можешь?")
                {
                    wea = false;
                    var answ1 = await client.SendTextMessageAsync(
                        chatId: msg.Chat.Id,
                        "Как что? Я отправляю тебе погоду!",
                        replyToMessageId: msg.MessageId,
                        replyMarkup: GetButtons());
                    wea = true;
                }
                        
                    else if(msg.Text== "Отправь мне погоду")
                {
                    wea = false;
                    var weat = await client.SendTextMessageAsync(
                        chatId: msg.Chat.Id,
                        "Просто напиши название города",
                        replyToMessageId: msg.MessageId,
                        replyMarkup: GetButtons());
                    wea = true;
                }
                        else if (msg.Text != "Отправь мне погоду" || msg.Text != "Что ты можешь?" || msg.Text != "Привет")
                        {
                            try
                            {
                                string url = $"http://api.openweathermap.org/data/2.5/weather?q={i}&lang=ru&units=metric&appid=3603bce5e85b1dbea7a262b009968846";
                                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                                HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                                string response;
                                using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
                                {
                                    response = streamReader.ReadToEnd();
                                }

                                WeatherResponse weatherResponse = JsonConvert.DeserializeObject<WeatherResponse>(response);
                                Console.WriteLine(response);
                                Console.WriteLine("Температура в {0}: {1}°C", weatherResponse.Name, weatherResponse.Main.temp);
                                

                                var weather11 = await client.SendTextMessageAsync(
                                            chatId: msg.Chat.Id,
                                            $"Температура в {weatherResponse.Name}: {Math.Round(weatherResponse.Main.temp)}°C, {weatherResponse.Weather[0].Description}",
                                            replyToMessageId: msg.MessageId,
                                            replyMarkup: GetButtons());
                                            var weather1 = await client.SendTextMessageAsync(
                                            chatId: msg.Chat.Id,
                                            $"https://openweathermap.org/city/{weatherResponse.id}",
                                            replyMarkup: GetButtons());
                        wea = false;
                            }
                            catch (WebException)
                            {
                        var exep = await client.SendTextMessageAsync(
                                    chatId: msg.Chat.Id,
                                    "Не понимаю",
                                    replyToMessageId: msg.MessageId,
                                    replyMarkup: GetButtons());
                    }
                        }
                else
                {
                    var exep1 = await client.SendTextMessageAsync(
                                   chatId: msg.Chat.Id,
                                   "Не понимаю",
                                   replyToMessageId: msg.MessageId,
                                   replyMarkup: GetButtons());
                }
            }
        }

        private static IReplyMarkup GetButtons()
        {
            return new ReplyKeyboardMarkup
            {
                Keyboard = new List<List<KeyboardButton>>
                {
                    new List<KeyboardButton>{new KeyboardButton { Text="Привет"}, new KeyboardButton { Text = "Что ты можешь?"} },
                    new List<KeyboardButton>{new KeyboardButton { Text="Отправь мне погоду"}}
                }
            };
        }
    }
}
