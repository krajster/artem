﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace calc
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public float c, d, i, g;
        public char o;
        public MainWindow()
        {
            InitializeComponent();
            copyres.Visibility = Visibility.Hidden;
            tb1.IsReadOnly = true;
        }
        private void _1_Click(object sender, RoutedEventArgs e)
        {
            tb1.Text = tb1.Text + 1;
            copyres.Visibility = Visibility.Hidden;
        }

        private void _2_Click(object sender, RoutedEventArgs e)
        {
            tb1.Text = tb1.Text + 2;
            copyres.Visibility = Visibility.Hidden;
        }

        private void _3_Click(object sender, RoutedEventArgs e)
        {
            tb1.Text = tb1.Text + 3;
            copyres.Visibility = Visibility.Hidden;
        }
        private void _4_Click_1(object sender, RoutedEventArgs e)
        {
            tb1.Text = tb1.Text + 4;
            copyres.Visibility = Visibility.Hidden;
        }
        private void _5_Click(object sender, RoutedEventArgs e)
        {
            tb1.Text = tb1.Text + 5;
            copyres.Visibility = Visibility.Hidden;
        }

        private void _6_Click(object sender, RoutedEventArgs e)
        {
            tb1.Text = tb1.Text + 6;
            copyres.Visibility = Visibility.Hidden;
        }

        private void _7_Click(object sender, RoutedEventArgs e)
        {
            tb1.Text = tb1.Text + 7;
            copyres.Visibility = Visibility.Hidden;
        }

        private void _8_Click(object sender, RoutedEventArgs e)
        {
            tb1.Text = tb1.Text + 8;
            copyres.Visibility = Visibility.Hidden;
        }

        private void _9_Click(object sender, RoutedEventArgs e)
        {
            tb1.Text = tb1.Text + 9;
            copyres.Visibility = Visibility.Hidden;
        }

        private void _0_Click(object sender, RoutedEventArgs e)
        {
            tb1.Text = tb1.Text + 0;
            copyres.Visibility = Visibility.Hidden;
        }

        private void C_Click(object sender, RoutedEventArgs e)
        {
            int lenght = tb1.Text.Length - 1;
            string text = tb1.Text;
            tb1.Clear();
            for (int i = 0; i < lenght; i++)
            {
                tb1.Text = tb1.Text + text[i];
            }
            copyres.Visibility = Visibility.Hidden;
        }

        private void AC_Click(object sender, RoutedEventArgs e)
        {
            tb1.Text = "";
            copyres.Visibility = Visibility.Hidden;
        }

        private void umn_Click(object sender, RoutedEventArgs e)
        {
            if (tb1.Text.Contains("-") || tb1.Text.Contains("/") || tb1.Text.Contains("*") || tb1.Text.Contains("+") || tb1.Text.Contains("%") || tb1.Text.Contains("²") || tb1.Text.Contains("√"))
            {
                if (o == '-')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '-' });
                    if (words[0] == "")
                    {
                        g = Convert.ToSingle(words[2]);
                        d = Convert.ToSingle(words[1]);
                        i = 0 - d - g;
                    }
                    else
                    {
                        c = Convert.ToSingle(words[0]);
                        d = Convert.ToSingle(words[1]);
                        i = c - d;
                    }
                }

                if (o == '+')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '+' });

                    c = Convert.ToSingle(words[0]);
                    d = Convert.ToSingle(words[1]);
                    i = c + d;
                }

                if (o == '*')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '*' });
                    if (words[1] == "") { }
                    c = Convert.ToSingle(words[0]);
                    d = Convert.ToSingle(words[1]);
                    i = c * d;
                }

                if (o == '/')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '/' });
                    c = Convert.ToSingle(words[0]);
                    d = Convert.ToSingle(words[1]);
                    i = c / d;
                }

                if (o == '²')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '²' });

                    c = Convert.ToSingle(words[0]);
                    i = c * c;
                }

                if (o == '√')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '√' });

                    c = Convert.ToSingle(words[1]);
                    i = (float)Math.Sqrt(c);
                }

                if (o == '%')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '%' });

                    c = Convert.ToSingle(words[0]);
                    d = Convert.ToSingle(words[1]);
                    i = d / 100 * c;
                }

                tb1.Text = "";
                tb1.Text = tb1.Text + i;
                o = '*';
                tb1.Text = tb1.Text + "*";

            }
            else
            {
                o = '*';
                tb1.Text = tb1.Text + "*";
            }
            copyres.Visibility = Visibility.Hidden;
        }

        private void del_Click(object sender, RoutedEventArgs e)
        {
            if (tb1.Text.Contains("-") || tb1.Text.Contains("/") || tb1.Text.Contains("*") || tb1.Text.Contains("+") || tb1.Text.Contains("%") || tb1.Text.Contains("²") || tb1.Text.Contains("√"))
            {
                if (o == '-')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '-' });
                    if (words[0] == "")
                    {
                        g = Convert.ToSingle(words[2]);
                        d = Convert.ToSingle(words[1]);
                        i = 0 - d - g;
                    }
                    else
                    {
                        c = Convert.ToSingle(words[0]);
                        d = Convert.ToSingle(words[1]);
                        i = c - d;
                    }
                }

                if (o == '+')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '+' });

                    c = Convert.ToSingle(words[0]);
                    d = Convert.ToSingle(words[1]);
                    i = c + d;
                }

                if (o == '*')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '*' });

                    c = Convert.ToSingle(words[0]);
                    d = Convert.ToSingle(words[1]);
                    i = c * d;
                }

                if (o == '/')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '/' });
                    if (words[1] == "") { }
                    c = Convert.ToSingle(words[0]);
                    d = Convert.ToSingle(words[1]);
                    i = c / d;
                }

                if (o == '²')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '²' });

                    c = Convert.ToSingle(words[0]);
                    i = c * c;
                }

                if (o == '√')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '√' });

                    c = Convert.ToSingle(words[1]);
                    i = (float)Math.Sqrt(c);
                }

                if (o == '%')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '%' });

                    c = Convert.ToSingle(words[0]);
                    d = Convert.ToSingle(words[1]);
                    i = d / 100 * c;
                }

                tb1.Text = "";
                tb1.Text = tb1.Text + i;
                o = '/';
                tb1.Text = tb1.Text + "/";

            }
            else
            {
                o = '/';
                tb1.Text = tb1.Text + "/";
            }
            copyres.Visibility = Visibility.Hidden;
        }

        private void min_Click(object sender, RoutedEventArgs e)
        {
            if (tb1.Text.Contains("-") || tb1.Text.Contains("/") || tb1.Text.Contains("*") || tb1.Text.Contains("+") || tb1.Text.Contains("%") || tb1.Text.Contains("²") || tb1.Text.Contains("√"))
            {
                if (o=='-')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '-' });
                    if (words[1] == "") { }
                    if (words[0] == "")
                    {
                        d = Convert.ToSingle(words[1]);
                        g = Convert.ToSingle(words[2]);
                        i = 0 - d - g;
                    }
                    else
                    {
                        c = Convert.ToSingle(words[0]);
                        d = Convert.ToSingle(words[1]);
                        i = c - d;
                    }
                }

                if (o == '+')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '+' });

                    c = Convert.ToSingle(words[0]);
                    d = Convert.ToSingle(words[1]);
                    i = c + d;
                }

                if (o == '*')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '*' });

                    c = Convert.ToSingle(words[0]);
                    d = Convert.ToSingle(words[1]);
                    i = c * d;
                }

                if (o == '/')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '/' });

                    c = Convert.ToSingle(words[0]);
                    d = Convert.ToSingle(words[1]);
                    i = c / d;
                }

                if (o == '²')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '²' });

                    c = Convert.ToSingle(words[0]);
                    i = c * c;
                }

                if (o == '√')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '√' });

                    c = Convert.ToSingle(words[1]);
                    i = (float)Math.Sqrt(c);
                }

                if (o == '%')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '%' });

                    c = Convert.ToSingle(words[0]);
                    d = Convert.ToSingle(words[1]);
                    i = d / 100 * c;
                }

                tb1.Text = "";
                tb1.Text = tb1.Text + i;
                o = '-';
                tb1.Text = tb1.Text + "-";

            }
            else
            {
                o = '-';
                tb1.Text = tb1.Text + "-";
            }
            copyres.Visibility = Visibility.Hidden;
        }

        private void pl_Click(object sender, RoutedEventArgs e)
        {
            if (tb1.Text.Contains("-") || tb1.Text.Contains("/") || tb1.Text.Contains("*") || tb1.Text.Contains("+") || tb1.Text.Contains("%") || tb1.Text.Contains("²") || tb1.Text.Contains("√"))
            {
                if (o == '+')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '+' });
                    if (words[1] == "") { }
                    c = Convert.ToSingle(words[0]);
                    d = Convert.ToSingle(words[1]);
                    i = c + d;
                    tb1.Text = tb1.Text + " = " + i;
                }

                if (o == '-')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '-' });
                    if (words[0] == "")
                    {
                        g = Convert.ToSingle(words[2]);
                        d = Convert.ToSingle(words[1]);
                        i = 0 - d - g;
                    }
                    else
                    {
                        c = Convert.ToSingle(words[0]);
                        d = Convert.ToSingle(words[1]);
                        i = c - d;
                    }
                }

                if (o == '*')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '*' });

                    c = Convert.ToSingle(words[0]);
                    d = Convert.ToSingle(words[1]);
                    i = c * d;
                }

                if (o == '/')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '/' });

                    c = Convert.ToSingle(words[0]);
                    d = Convert.ToSingle(words[1]);
                    i = c / d;
                }

                if (o == '²')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '²' });

                    c = Convert.ToSingle(words[0]);
                    i = c * c;
                }

                if (o == '√')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '√' });

                    c = Convert.ToSingle(words[1]);
                    i = (float)Math.Sqrt(c);
                }

                if (o == '%')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '%' });

                    c = Convert.ToSingle(words[0]);
                    d = Convert.ToSingle(words[1]);
                    i = d / 100 * c;
                }

                tb1.Text = "";
                tb1.Text = tb1.Text + i;
                o = '+';
                tb1.Text = tb1.Text + "+";

            }
            else
            {
                o = '+';
                tb1.Text = tb1.Text + "+";
            }
        }

        private void kk_Click(object sender, RoutedEventArgs e)
        {
            if (tb1.Text.Contains("-") || tb1.Text.Contains("/") || tb1.Text.Contains("*") || tb1.Text.Contains("+") || tb1.Text.Contains("%") || tb1.Text.Contains("²") || tb1.Text.Contains("√"))
            {
                if (o == '+')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '+' });
                    c = Convert.ToSingle(words[0]);
                    d = Convert.ToSingle(words[1]);
                    i = c + d;
                    tb1.Text = tb1.Text + " = " + i;
                }

                if (o == '-')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '-' });
                    if (words[0] == "")
                    {
                        g = Convert.ToSingle(words[2]);
                        d = Convert.ToSingle(words[1]);
                        i = 0 - d - g;
                    }
                    else
                    {
                        c = Convert.ToSingle(words[0]);
                        d = Convert.ToSingle(words[1]);
                        i = c - d;
                    }
                }

                if (o == '*')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '*' });

                    c = Convert.ToSingle(words[0]);
                    d = Convert.ToSingle(words[1]);
                    i = c * d;
                }

                if (o == '/')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '/' });

                    c = Convert.ToSingle(words[0]);
                    d = Convert.ToSingle(words[1]);
                    i = c / d;
                }

                if (o == '²')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '²' });

                    c = Convert.ToSingle(words[0]);
                    i = c * c;
                }

                if (o == '√')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '√' });
                    if (words[1] == "") { }
                    c = Convert.ToSingle(words[1]);
                    i = (float)Math.Sqrt(c);
                }

                if (o == '%')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '%' });

                    c = Convert.ToSingle(words[0]);
                    d = Convert.ToSingle(words[1]);
                    i = d / 100 * c;
                }

                tb1.Text = "";

                tb1.Text = tb1.Text + "√";
                tb1.Text = tb1.Text + i;
                o = '√';

            }
            else
            {
                o = '√';
                tb1.Text = tb1.Text + "√";
            }
            copyres.Visibility = Visibility.Hidden;
        }

        private void kv_Click(object sender, RoutedEventArgs e)
        {
            if (tb1.Text.Contains("-") || tb1.Text.Contains("/") || tb1.Text.Contains("*") || tb1.Text.Contains("+") || tb1.Text.Contains("%") || tb1.Text.Contains("²") || tb1.Text.Contains("√"))
            {
                if (o == '+')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '+' });
                    c = Convert.ToSingle(words[0]);
                    d = Convert.ToSingle(words[1]);
                    i = c + d;
                    tb1.Text = tb1.Text + " = " + i;
                }

                if (o == '-')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '-' });
                    if (words[0] == "")
                    {
                        g = Convert.ToSingle(words[2]);
                        d = Convert.ToSingle(words[1]);
                        i = 0 - d - g;
                    }
                    else
                    {
                        c = Convert.ToSingle(words[0]);
                        d = Convert.ToSingle(words[1]);
                        i = c - d;
                    }
                }

                if (o == '*')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '*' });

                    c = Convert.ToSingle(words[0]);
                    d = Convert.ToSingle(words[1]);
                    i = c * d;
                }

                if (o == '/')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '/' });

                    c = Convert.ToSingle(words[0]);
                    d = Convert.ToSingle(words[1]);
                    i = c / d;
                }

                if (o == '²')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '²' });
                    if (words[1] == "") { }
                    c = Convert.ToSingle(words[0]);
                    i = c * c;
                }

                if (o == '√')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '√' });

                    c = Convert.ToSingle(words[1]);
                    i = (float)Math.Sqrt(c);
                }

                if (o == '%')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '%' });

                    c = Convert.ToSingle(words[0]);
                    d = Convert.ToSingle(words[1]);
                    i = d / 100 * c;
                }

                tb1.Text = "";
                tb1.Text = tb1.Text + i;
                o = '²';
                tb1.Text = tb1.Text + "²";

            }
            else
            {
                o = '²';
                tb1.Text = tb1.Text + "²";
            }
            copyres.Visibility = Visibility.Hidden;
        }

        private void pr_Click(object sender, RoutedEventArgs e)
        {
            if (tb1.Text.Contains("-") || tb1.Text.Contains("/") || tb1.Text.Contains("*") || tb1.Text.Contains("+") || tb1.Text.Contains("%") || tb1.Text.Contains("²") || tb1.Text.Contains("√"))
            {
                if (o == '+')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '+' });
                    c = Convert.ToSingle(words[0]);
                    d = Convert.ToSingle(words[1]);
                    i = c + d;
                    tb1.Text = tb1.Text + " = " + i;
                }

                if (o == '-')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '-' });
                    if (words[0] == "")
                    {
                        g = Convert.ToSingle(words[2]);
                        d = Convert.ToSingle(words[1]);
                        i = 0 - d - g;
                    }
                    else
                    {
                        c = Convert.ToSingle(words[0]);
                        d = Convert.ToSingle(words[1]);
                        i = c - d;
                    }
                }

                if (o == '*')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '*' });

                    c = Convert.ToSingle(words[0]);
                    d = Convert.ToSingle(words[1]);
                    i = c * d;
                }

                if (o == '/')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '/' });

                    c = Convert.ToSingle(words[0]);
                    d = Convert.ToSingle(words[1]);
                    i = c / d;
                }

                if (o == '²')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '²' });

                    c = Convert.ToSingle(words[0]);
                    i = c * c;
                }

                if (o == '√')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '√' });

                    c = Convert.ToSingle(words[1]);
                    i = (float)Math.Sqrt(c);
                }

                if (o == '%')
                {
                    string beta = tb1.Text;
                    string[] words = beta.Split(new char[] { '%' });
                    if (words[1] == "") { }
                    c = Convert.ToSingle(words[0]);
                    d = Convert.ToSingle(words[1]);
                    i = d / 100 * c;
                }

                tb1.Text = "";
                tb1.Text = tb1.Text + i;
                o = '%';
                tb1.Text = tb1.Text + "%";

            }
            else
            {
                o = '%';
                tb1.Text = tb1.Text + "%";
            }
            copyres.Visibility = Visibility.Hidden;
        }
        private void ravn_Click(object sender, RoutedEventArgs e)
        {
            copyres.Visibility = Visibility.Visible;
            if (o == ' ')
            {
                string beta = tb1.Text;
                string[] words = beta.Split(new char[] { ' ' });

                c = Convert.ToSingle(words[0]);
                i = c;
                tb1.Text = tb1.Text + " = " + i;
            }
            if (o == '+')
            {
                string beta = tb1.Text;
                string[] words = beta.Split(new char[] { '+' });

                c = Convert.ToSingle(words[0]);
                d = Convert.ToSingle(words[1]);
                i = c + d;
                tb1.Text = tb1.Text + " = " + i;
            }

            if (o == '*')
            {
                string beta = tb1.Text;
                string[] words = beta.Split(new char[] { '*' });

                c = Convert.ToSingle(words[0]);
                d = Convert.ToSingle(words[1]);
                i = c * d;
                tb1.Text = tb1.Text + " = " + i;
            }

            if (o == '/')
            {
                string beta = tb1.Text;
                string[] words = beta.Split(new char[] { '/' });

                c = Convert.ToSingle(words[0]);
                d = Convert.ToSingle(words[1]);
                i = c / d;
                tb1.Text = tb1.Text + " = " + i;
            }
            if (o == '-')
            {
                string beta = tb1.Text;
                string[] words = beta.Split(new char[] { '-' });
                if (words[0]=="")
                {
                    g = Convert.ToSingle(words[2]);
                    d = Convert.ToSingle(words[1]);
                    i = 0 - d - g;
                }
                else
                {
                    c = Convert.ToSingle(words[0]);
                    d = Convert.ToSingle(words[1]);
                    i = c - d;
                }
                tb1.Text = tb1.Text + " = " + i;
            }

            if (o == '²')
            {
                string beta = tb1.Text;
                string[] words = beta.Split(new char[] { '²' });

                c = Convert.ToSingle(words[0]);
                i = c * c;
                tb1.Text = tb1.Text + " = " + i;
            }

            if (o == '√')
            {
                string beta = tb1.Text;
                string[] words = beta.Split(new char[] { '√' });

                c = Convert.ToSingle(words[1]);
                i = (float)Math.Sqrt(c);
                tb1.Text = tb1.Text + " = " + i;
            }

            if (o == '%')
            {
                string beta = tb1.Text;
                string[] words = beta.Split(new char[] { '%' });

                c = Convert.ToSingle(words[0]);
                d = Convert.ToSingle(words[1]);
                i = d / 100 * c;
                tb1.Text = tb1.Text + " = " + i;
            }
        }

            private void tb1_KeyDown(object sender, KeyEventArgs e)
            {

            }

            private void kla(object sender, KeyEventArgs e)
            {
                if (e.Key == System.Windows.Input.Key.Enter)
                {

                {
                    copyres.Visibility = Visibility.Visible;
                    if (o == ' ')
                    {
                        string beta = tb1.Text;
                        string[] words = beta.Split(new char[] { ' ' });

                        c = Convert.ToSingle(words[0]);
                        i = c;
                        tb1.Text = tb1.Text + " = " + i;
                    }
                    if (o == '+')
                    {
                        string beta = tb1.Text;
                        string[] words = beta.Split(new char[] { '+' });

                        c = Convert.ToSingle(words[0]);
                        d = Convert.ToSingle(words[1]);
                        i = c + d;
                        tb1.Text = tb1.Text + " = " + i;
                    }

                    if (o == '*')
                    {
                        string beta = tb1.Text;
                        string[] words = beta.Split(new char[] { '*' });

                        c = Convert.ToSingle(words[0]);
                        d = Convert.ToSingle(words[1]);
                        i = c * d;
                        tb1.Text = tb1.Text + " = " + i;
                    }

                    if (o == '/')
                    {
                        string beta = tb1.Text;
                        string[] words = beta.Split(new char[] { '/' });

                        c = Convert.ToSingle(words[0]);
                        d = Convert.ToSingle(words[1]);
                        i = c / d;
                        tb1.Text = tb1.Text + " = " + i;
                    }
                    if (o == '-')
                    {
                        string beta = tb1.Text;
                        string[] words = beta.Split(new char[] { '-' });
                        if (words[0] == "")
                        {
                            g = Convert.ToSingle(words[2]);
                            d = Convert.ToSingle(words[1]);
                            i = 0 - d - g;
                        }
                        else
                        {
                            c = Convert.ToSingle(words[0]);
                            d = Convert.ToSingle(words[1]);
                            i = c - d;
                        }
                        tb1.Text = tb1.Text + " = " + i;
                    }

                    if (o == '²')
                    {
                        string beta = tb1.Text;
                        string[] words = beta.Split(new char[] { '²' });

                        c = Convert.ToSingle(words[0]);
                        i = c * c;
                        tb1.Text = tb1.Text + " = " + i;
                    }

                    if (o == '√')
                    {
                        string beta = tb1.Text;
                        string[] words = beta.Split(new char[] { '√' });

                        c = Convert.ToSingle(words[1]);
                        i = (float)Math.Sqrt(c);
                        tb1.Text = tb1.Text + " = " + i;
                    }

                    if (o == '%')
                    {
                        string beta = tb1.Text;
                        string[] words = beta.Split(new char[] { '%' });

                        c = Convert.ToSingle(words[0]);
                        d = Convert.ToSingle(words[1]);
                        i = d / 100 * c;
                        tb1.Text = tb1.Text + " = " + i;
                    }
                }
            }
                if (e.Key == System.Windows.Input.Key.Escape)
                {
                    tb1.Text = "";
                    copyres.Visibility = Visibility.Hidden;
                }
                if (e.Key == System.Windows.Input.Key.Back)
                {
                    int lenght = tb1.Text.Length - 1;
                    string text = tb1.Text;
                    tb1.Clear();
                    for (int i = 0; i < lenght; i++)
                    {
                        tb1.Text = tb1.Text + text[i];
                    }
                    copyres.Visibility = Visibility.Hidden;
                }
                if (e.Key == System.Windows.Input.Key.Divide)
            {
                if (tb1.Text.Contains("-") || tb1.Text.Contains("/") || tb1.Text.Contains("*") || tb1.Text.Contains("+") || tb1.Text.Contains("%") || tb1.Text.Contains("²") || tb1.Text.Contains("√"))
                {
                    if (o == '-')
                    {
                        string beta = tb1.Text;
                        string[] words = beta.Split(new char[] { '-' });
                        if (words[0] == "")
                        {
                            g = Convert.ToSingle(words[2]);
                            d = Convert.ToSingle(words[1]);
                            i = 0 - d - g;
                        }
                        else
                        {
                            c = Convert.ToSingle(words[0]);
                            d = Convert.ToSingle(words[1]);
                            i = c - d;
                        }
                    }

                    if (o == '+')
                    {
                        string beta = tb1.Text;
                        string[] words = beta.Split(new char[] { '+' });

                        c = Convert.ToSingle(words[0]);
                        d = Convert.ToSingle(words[1]);
                        i = c + d;
                    }

                    if (o == '*')
                    {
                        string beta = tb1.Text;
                        string[] words = beta.Split(new char[] { '*' });

                        c = Convert.ToSingle(words[0]);
                        d = Convert.ToSingle(words[1]);
                        i = c * d;
                    }

                    if (o == '/')
                    {
                        string beta = tb1.Text;
                        string[] words = beta.Split(new char[] { '/' });
                        if (words[1] == "") { }
                        c = Convert.ToSingle(words[0]);
                        d = Convert.ToSingle(words[1]);
                        i = c / d;
                    }

                    if (o == '²')
                    {
                        string beta = tb1.Text;
                        string[] words = beta.Split(new char[] { '²' });

                        c = Convert.ToSingle(words[0]);
                        i = c * c;
                    }

                    if (o == '√')
                    {
                        string beta = tb1.Text;
                        string[] words = beta.Split(new char[] { '√' });

                        c = Convert.ToSingle(words[1]);
                        i = (float)Math.Sqrt(c);
                    }

                    if (o == '%')
                    {
                        string beta = tb1.Text;
                        string[] words = beta.Split(new char[] { '%' });

                        c = Convert.ToSingle(words[0]);
                        d = Convert.ToSingle(words[1]);
                        i = d / 100 * c;
                    }

                    tb1.Text = "";
                    tb1.Text = tb1.Text + i;
                    o = '/';
                    tb1.Text = tb1.Text + "/";

                }
                else
                {
                    o = '/';
                    tb1.Text = tb1.Text + "/";
                }
                copyres.Visibility = Visibility.Hidden;
            }
                if (e.Key == System.Windows.Input.Key.Subtract || e.Key == Key.OemMinus)
            {
                if (tb1.Text.Contains("-") || tb1.Text.Contains("/") || tb1.Text.Contains("*") || tb1.Text.Contains("+") || tb1.Text.Contains("%") || tb1.Text.Contains("²") || tb1.Text.Contains("√"))
                {
                    if (o == '-')
                    {
                        string beta = tb1.Text;
                        string[] words = beta.Split(new char[] { '-' });
                        if (words[1] == "") { }
                        if (words[0] == "")
                        {
                            g = Convert.ToSingle(words[2]);
                            d = Convert.ToSingle(words[1]);
                            i = 0 - d - g;
                        }
                        else
                        {
                            c = Convert.ToSingle(words[0]);
                            d = Convert.ToSingle(words[1]);
                            i = c - d;
                        }
                    }

                    if (o == '+')
                    {
                        string beta = tb1.Text;
                        string[] words = beta.Split(new char[] { '+' });

                        c = Convert.ToSingle(words[0]);
                        d = Convert.ToSingle(words[1]);
                        i = c + d;
                    }

                    if (o == '*')
                    {
                        string beta = tb1.Text;
                        string[] words = beta.Split(new char[] { '*' });

                        c = Convert.ToSingle(words[0]);
                        d = Convert.ToSingle(words[1]);
                        i = c * d;
                    }

                    if (o == '/')
                    {
                        string beta = tb1.Text;
                        string[] words = beta.Split(new char[] { '/' });

                        c = Convert.ToSingle(words[0]);
                        d = Convert.ToSingle(words[1]);
                        i = c / d;
                    }

                    if (o == '²')
                    {
                        string beta = tb1.Text;
                        string[] words = beta.Split(new char[] { '²' });

                        c = Convert.ToSingle(words[0]);
                        i = c * c;
                    }

                    if (o == '√')
                    {
                        string beta = tb1.Text;
                        string[] words = beta.Split(new char[] { '√' });

                        c = Convert.ToSingle(words[1]);
                        i = (float)Math.Sqrt(c);
                    }

                    if (o == '%')
                    {
                        string beta = tb1.Text;
                        string[] words = beta.Split(new char[] { '%' });

                        c = Convert.ToSingle(words[0]);
                        d = Convert.ToSingle(words[1]);
                        i = d / 100 * c;
                    }

                    tb1.Text = "";
                    tb1.Text = tb1.Text + i;
                    o = '-';
                    tb1.Text = tb1.Text + "-";

                }
                else
                {
                    o = '-';
                    tb1.Text = tb1.Text + "-";
                }
                copyres.Visibility = Visibility.Hidden;
            }
                if (e.Key == System.Windows.Input.Key.OemPlus || e.Key == Key.Add)
            {
                if (tb1.Text.Contains("-") || tb1.Text.Contains("/") || tb1.Text.Contains("*") || tb1.Text.Contains("+") || tb1.Text.Contains("%") || tb1.Text.Contains("²") || tb1.Text.Contains("√"))
                {
                    if (o == '+')
                    {
                        string beta = tb1.Text;
                        string[] words = beta.Split(new char[] { '+' });
                        if (words[1] == "") { }
                        c = Convert.ToSingle(words[0]);
                        d = Convert.ToSingle(words[1]);
                        i = c + d;
                        tb1.Text = tb1.Text + " = " + i;
                    }

                    if (o == '-')
                    {
                        string beta = tb1.Text;
                        string[] words = beta.Split(new char[] { '-' });
                        if (words[0] == "")
                        {
                            g = Convert.ToSingle(words[2]);
                            d = Convert.ToSingle(words[1]);
                            i = 0 - d - g;
                        }
                        else
                        {
                            c = Convert.ToSingle(words[0]);
                            d = Convert.ToSingle(words[1]);
                            i = c - d;
                        }
                    }

                    if (o == '*')
                    {
                        string beta = tb1.Text;
                        string[] words = beta.Split(new char[] { '*' });

                        c = Convert.ToSingle(words[0]);
                        d = Convert.ToSingle(words[1]);
                        i = c * d;
                    }

                    if (o == '/')
                    {
                        string beta = tb1.Text;
                        string[] words = beta.Split(new char[] { '/' });

                        c = Convert.ToSingle(words[0]);
                        d = Convert.ToSingle(words[1]);
                        i = c / d;
                    }

                    if (o == '²')
                    {
                        string beta = tb1.Text;
                        string[] words = beta.Split(new char[] { '²' });

                        c = Convert.ToSingle(words[0]);
                        i = c * c;
                    }

                    if (o == '√')
                    {
                        string beta = tb1.Text;
                        string[] words = beta.Split(new char[] { '√' });

                        c = Convert.ToSingle(words[1]);
                        i = (float)Math.Sqrt(c);
                    }

                    if (o == '%')
                    {
                        string beta = tb1.Text;
                        string[] words = beta.Split(new char[] { '%' });

                        c = Convert.ToSingle(words[0]);
                        d = Convert.ToSingle(words[1]);
                        i = d / 100 * c;
                    }

                    tb1.Text = "";
                    tb1.Text = tb1.Text + i;
                    o = '+';
                    tb1.Text = tb1.Text + "+";

                }
                else
                {
                    o = '+';
                    tb1.Text = tb1.Text + "+";
                }
            }
                if (e.Key == System.Windows.Input.Key.Multiply)
            {
                if (tb1.Text.Contains("-") || tb1.Text.Contains("/") || tb1.Text.Contains("*") || tb1.Text.Contains("+") || tb1.Text.Contains("%") || tb1.Text.Contains("²") || tb1.Text.Contains("√"))
                {
                    if (o == '-')
                    {
                        string beta = tb1.Text;
                        string[] words = beta.Split(new char[] { '-' });
                        if (words[0] == "")
                        {
                            g = Convert.ToSingle(words[2]);
                            d = Convert.ToSingle(words[1]);
                            i = 0 - d - g;
                        }
                        else
                        {
                            c = Convert.ToSingle(words[0]);
                            d = Convert.ToSingle(words[1]);
                            i = c - d;
                        }
                    }

                    if (o == '+')
                    {
                        string beta = tb1.Text;
                        string[] words = beta.Split(new char[] { '+' });

                        c = Convert.ToSingle(words[0]);
                        d = Convert.ToSingle(words[1]);
                        i = c + d;
                    }

                    if (o == '*')
                    {
                        string beta = tb1.Text;
                        string[] words = beta.Split(new char[] { '*' });
                        if (words[1] == "") { }
                        c = Convert.ToSingle(words[0]);
                        d = Convert.ToSingle(words[1]);
                        i = c * d;
                    }

                    if (o == '/')
                    {
                        string beta = tb1.Text;
                        string[] words = beta.Split(new char[] { '/' });
                        c = Convert.ToSingle(words[0]);
                        d = Convert.ToSingle(words[1]);
                        i = c / d;
                    }

                    if (o == '²')
                    {
                        string beta = tb1.Text;
                        string[] words = beta.Split(new char[] { '²' });

                        c = Convert.ToSingle(words[0]);
                        i = c * c;
                    }

                    if (o == '√')
                    {
                        string beta = tb1.Text;
                        string[] words = beta.Split(new char[] { '√' });

                        c = Convert.ToSingle(words[1]);
                        i = (float)Math.Sqrt(c);
                    }

                    if (o == '%')
                    {
                        string beta = tb1.Text;
                        string[] words = beta.Split(new char[] { '%' });

                        c = Convert.ToSingle(words[0]);
                        d = Convert.ToSingle(words[1]);
                        i = d / 100 * c;
                    }

                    tb1.Text = "";
                    tb1.Text = tb1.Text + i;
                    o = '*';
                    tb1.Text = tb1.Text + "*";

                }
                else
                {
                    o = '*';
                    tb1.Text = tb1.Text + "*";
                }
                copyres.Visibility = Visibility.Hidden;
            }
                if (e.Key == System.Windows.Input.Key.D1 || e.Key == Key.NumPad1)
                {
                    tb1.Text = tb1.Text + 1;
                    copyres.Visibility = Visibility.Hidden;
                }
                if (e.Key == System.Windows.Input.Key.D2 || e.Key == Key.NumPad2)
                {
                    tb1.Text = tb1.Text + 2;
                    copyres.Visibility = Visibility.Hidden;
                }
                if (e.Key == System.Windows.Input.Key.D3 || e.Key == Key.NumPad3)
                {
                    tb1.Text = tb1.Text + 3;
                    copyres.Visibility = Visibility.Hidden;
                }
                if (e.Key == System.Windows.Input.Key.D4 || e.Key == Key.NumPad4)
                {
                    tb1.Text = tb1.Text + 4;
                    copyres.Visibility = Visibility.Hidden;
                }
                if (e.Key == System.Windows.Input.Key.D5 || e.Key == Key.NumPad5)
                {
                    tb1.Text = tb1.Text + 5;
                    copyres.Visibility = Visibility.Hidden;
                }
                if (e.Key == System.Windows.Input.Key.D6 || e.Key == Key.NumPad6)
                {
                    tb1.Text = tb1.Text + 6;
                    copyres.Visibility = Visibility.Hidden;
                }
                if (e.Key == System.Windows.Input.Key.D7 || e.Key == Key.NumPad7)
                {
                    tb1.Text = tb1.Text + 7;
                    copyres.Visibility = Visibility.Hidden;
                }
                if (e.Key == System.Windows.Input.Key.D8 || e.Key == Key.NumPad8)
                {
                    tb1.Text = tb1.Text + 8;
                    copyres.Visibility = Visibility.Hidden;
                }
                if (e.Key == System.Windows.Input.Key.D9 || e.Key == Key.NumPad9)
                {
                    tb1.Text = tb1.Text + 9;
                    copyres.Visibility = Visibility.Hidden;
                }
                if (e.Key == System.Windows.Input.Key.D0 || e.Key == Key.NumPad0)
                {
                    tb1.Text = tb1.Text + 0;
                    copyres.Visibility = Visibility.Hidden;
                }
            }

        private void res_Click(object sender, RoutedEventArgs e)
        {
            string str = Convert.ToString(i);
            Clipboard.SetText(str);
        }
    }
}
